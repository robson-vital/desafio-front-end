// Validando senha
var $password = $('#txtSenha');
if($password.length > 0) {
    var result = '.validation';
    var $result = $(result);

    $password.after($result);
}   
    $(document).on('keyup', $password, function () {
        $result.html(checkStrength($password.val()));
    });

    function checkStrength(password) {
        var strength = 0;
        if (password.length >= 6) {
            $result.removeClass();  
            $(".tips").find('#tip1').addClass('vrystrongpass');
        }else {
            $(".tips").find('#tip1').removeClass();
            $(".tips").find('#tip1').addClass('goodpass');
        }

        if (password.length > 7){
            strength += 1;
        }
    
        if (password.match(/([A-Z])/)){
            strength += 1;
            $(".tips").find('#tip2').addClass('vrystrongpass');
        }else {
            $(".tips").find('#tip2').removeClass();
            $(".tips").find('#tip2').addClass('goodpass');
        }
      
        if (password.match(/([0-9])/)){
            strength += 1;
            $(".tips").find('#tip3').addClass('vrystrongpass');
        }else {
            $(".tips").find('#tip3').removeClass();
            $(".tips").find('#tip3').addClass('goodpass');
        }
           
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)){
            strength += 1;
        }
       
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)){
            strength += 1;  
        }
       
        if (strength < 2) {
            $result.removeClass();
            $result.addClass('stillweakpass');
            $password.addClass('border-stillweakpass');
       
        } else if (strength === 2) {
            $result.removeClass();
            $result.addClass('goodpass');
            $password.addClass('border-goodpass');
        
        } else if (strength === 3) {
            $result.removeClass();
            $result.addClass('vrystrongpass');
            $password.addClass('border-vrystrongpass');
         
        } else{
            $result.removeClass();
        }
    }